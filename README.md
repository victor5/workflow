
# 迁移至新仓库：[https://gitee.com/victor5/web-server.git](https://gitee.com/victor5/web-server.git)

# [前端仓库：https://gitee.com/victor5/web-html.git](https://gitee.com/victor5/web-html.git)

# workflow

#### 介绍
面向开发人员，超可配易集成的工作流引擎应用，为被集成而生，不侵入业务系统，配置灵活不依赖特定的人事数据结构，能解决80%以上流程类型，剩下的20%需要写一些胶水代码。是快速实现原型和深度定制化开发的首选。

#### 软件架构
流程引擎：golang
脚本引擎：golang
后台配置：java
前端UI：element-ui


#### 管理操作截图

![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/160613_db82db7c_9545954.jpeg "1.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/160622_51e14f3c_9545954.jpeg "2.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/160639_9133aed3_9545954.jpeg "3.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/160649_5f047d93_9545954.jpeg "4.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/160658_e4ace675_9545954.jpeg "5.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/160706_5d31bc8a_9545954.jpeg "6.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/160715_3c55e3fe_9545954.jpeg "7.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/160723_afa80045_9545954.jpeg "8.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/160733_487be6e4_9545954.jpeg "9.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/160741_4f3a5b26_9545954.jpeg "10.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/160751_ec74cf6b_9545954.jpeg "11.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/160803_08b6c8d7_9545954.jpeg "12.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/160812_d704d138_9545954.jpeg "13.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/160821_483ca5f1_9545954.jpeg "14.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/160838_4ebb910c_9545954.jpeg "15.jpg")